package tests;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.*;
import pages.AppointmentsPage;
import pages.Browser;
import pages.LoginPage;
import org.openqa.selenium.support.PageFactory;
import pages.NavigationBar;

import java.time.Duration;
import java.util.Random;

import static utils.Constants.browserName;
import static utils.property.Properties.getImplicitWait;


public class AppointmentsTest {

    static LoginPage loginPage;

    @Parameters({"browser"})
    @BeforeTest
    public void before(@Optional("Chrome") String browser) throws Exception {
        browserName = browser;
        Browser.getDriver().manage().timeouts()
                .implicitlyWait(Duration.ofSeconds(getImplicitWait()));
    }

    @AfterTest
    public void afterClass() {
//        Browser.quit();
    }

    @Test
    public void CreateAppointment() throws Exception {
        Random random = new Random();
        String appointmentTitle = "appointment" + random.nextInt(10000);
        int durationDays = random.nextInt(30);

        loginPage = PageFactory.initElements(Browser.getDriver(), LoginPage.class);
        NavigationBar navigationBar = PageFactory.initElements(Browser.getDriver(), NavigationBar.class);
        AppointmentsPage appointmentsPage = PageFactory.initElements(Browser.getDriver(), AppointmentsPage.class);

        loginPage.invoke(Browser.getDriver());
        loginPage.setUserName();
        loginPage.setPassword();
        loginPage.clickLogInButton();
        navigationBar.openAppointments();
        navigationBar.openCalendar();
        appointmentsPage.openNewAppointment();
        appointmentsPage.createAppointment(appointmentTitle, durationDays);
        boolean isCreated = appointmentsPage.checkNewAppointmentWasCreated(appointmentTitle, durationDays);
        Assert.assertTrue(isCreated, "Check appointment creation");
        Reporter.log("Appointment " + appointmentTitle + " was added to calendar");
    }
}
