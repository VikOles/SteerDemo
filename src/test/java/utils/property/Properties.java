package utils.property;

public class Properties {
    private static final ReadPropertyFile propertyReader = new ReadPropertyFile();

    public static String getUrl() throws Exception {
        return propertyReader.getPropertyValue(PropertyKeys.URL.getValue());
    }

    public static String getEmail() throws Exception {
        return propertyReader.getPropertyValue(PropertyKeys.EMAIL.getValue());
    }

    public static String getPassword() throws Exception {
        return propertyReader.getPropertyValue(PropertyKeys.PASSWORD.getValue());
    }

    public static int getImplicitWait() throws Exception {
        return propertyReader.getIntValue(PropertyKeys.IMPLICIT_WAIT.getValue());
    }

    public static int getExplicitWait() throws Exception {
        return propertyReader.getIntValue(PropertyKeys.EXPLICIT_WAIT.getValue());
    }

    public static boolean getIsRemote() throws Exception {
        return propertyReader.getBooleanValue(PropertyKeys.IS_REMOTE.getValue());
    }

}
