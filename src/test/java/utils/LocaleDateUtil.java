package utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.WeekFields;
import java.util.Locale;

public class LocaleDateUtil {
    private static final WeekFields weekFields = WeekFields.of(Locale.US);
    public static DateTimeFormatter yearMonthFormatter = DateTimeFormatter.ofPattern("yyyy-MM");
    public static DateTimeFormatter monthDayYearFormatter = DateTimeFormatter.ofPattern("MMM d, yyyy, ");

    public static int getUSDayOfWeek(LocalDateTime localDateTime) {
        return localDateTime.get(weekFields.dayOfWeek());
    }

    public static int getUSWeekOfMonth(LocalDateTime localDateTime) {
        return localDateTime.get(weekFields.weekOfMonth());
    }
}
