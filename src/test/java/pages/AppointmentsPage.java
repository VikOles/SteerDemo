package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

import static utils.LocaleDateUtil.*;

public class AppointmentsPage {
    @FindBy(xpath = "//button[descendant::span[text()='New Appointment']]")
    WebElement newAppointmentButton;

    @FindBy(xpath = "//span[text()='Create']")
    WebElement newAppCreateButton;

    @FindBy(id = "modal")
    WebElement createWindow;

    @FindBy(id = "create_anew_appointment")
    WebElement createWindowHeaderIcon;

    @FindBy(name = "title")
    WebElement createWindowTitleInput;

    @FindBy(xpath = "//div[@name='date']//input")
    WebElement createWindowDateInput;

    @FindBy(xpath = "//div[@class='undefined']//input[@class='']")
    List<WebElement> selectedTimeNewAppointmentWindow;

    @FindBy(xpath = "//button[text()='Today']/following-sibling::button")
    WebElement nextCalendarButton;

    @FindBy(xpath = "//div[@class='rbc-more-events-popper']//span")
    List<WebElement> calendarMoreEvents;

    @FindBy(className = "rbc-month-row")
    List<WebElement> calendarWeeks;

    WebDriverWait wait = new WebDriverWait(Browser.getDriver(), Duration.ofSeconds(10));
    WebDriverWait longWait = new WebDriverWait(Browser.getDriver(), Duration.ofSeconds(60));

    public AppointmentsPage() throws Exception {
    }

    public void openNewAppointment() {
        wait.until(ExpectedConditions.visibilityOf(newAppointmentButton));
        newAppointmentButton.click();
    }

    public void setAppTitle(String titleName) {
        wait.until(ExpectedConditions.visibilityOf(createWindow));
        createWindowTitleInput.sendKeys(titleName);
    }

    public void openCalendar() {
        createWindowDateInput.click();
    }

    public void setDateRange(int durationInDays) throws Exception {
        LocalDateTime startDateTime = LocalDateTime.now();
        LocalDateTime endDateTime = startDateTime.plusDays(durationInDays);

        selectDate(startDateTime);
        selectDate(endDateTime);

        StringBuilder dateRange = new StringBuilder();
        dateRange
                .append(startDateTime.format(monthDayYearFormatter))
                .append(selectedTimeNewAppointmentWindow.get(0).getAttribute("value"))
                .append(" - ")
                .append(endDateTime.format(monthDayYearFormatter))
                .append(selectedTimeNewAppointmentWindow.get(1).getAttribute("value"));
        createWindowHeaderIcon.click();
        Browser.getDriver().findElement(By.xpath(String.format("//input[@value='%s']", dateRange))).isDisplayed();
        Reporter.log("Appointment duration is" + dateRange);
    }

    private void selectDate(LocalDateTime localDateTime) throws Exception {
        String xpathDay = String.format("//div[@aria-label='month  %s']//div[text()='%d']",
                localDateTime.format(yearMonthFormatter),
                localDateTime.getDayOfMonth());
        longWait.until(webDriver -> ((JavascriptExecutor) webDriver)
                .executeScript("return document.readyState").equals("complete"));
        List<WebElement> date = Browser.getDriver().findElements(By.xpath(xpathDay));
        JavascriptExecutor executor = (JavascriptExecutor) Browser.getDriver();
        executor.executeScript("arguments[0].click();", date.get(0));

    }

    public void clickCreate() {
        wait.until(ExpectedConditions.elementToBeClickable(newAppCreateButton));
        newAppCreateButton.click();
    }

    public void createAppointment(String titleName, int durationInDays) throws Exception {
        setAppTitle(titleName);
        openCalendar();
        setDateRange(durationInDays);
        clickCreate();
        Reporter.log("[INFO] Appointment " + titleName + " was successfully created");
    }

    public boolean checkNewAppointmentWasCreated(String titleOfAppointment, int durationInDays) throws Exception {
        boolean isCreatedForFirstDay;
        boolean isCreatedForLastDay;

        LocalDateTime startDateTime = LocalDateTime.now();
        LocalDateTime endDateTime = startDateTime.plusDays(durationInDays);

        By appointment = By.xpath(String.format(".//span[text()='%s']/ancestor::div[@class='rbc-row-segment']", titleOfAppointment));
        By startDate = By.xpath(String.format(".//div/button[text()='%d']", startDateTime.getDayOfMonth()));
        By endDate = By.xpath(String.format(".//div/button[text()='%d']", endDateTime.getDayOfMonth()));
        WebElement currentWeek = calendarWeeks.get(getUSWeekOfMonth(startDateTime) - 1);
        if (currentWeek.findElements(appointment).size()==0) {
            System.out.println("Check appointment in More");
            return checkAppointmentInMoreSection(titleOfAppointment, startDateTime);
        }
        WebElement appointmentWebElement = currentWeek.findElement(appointment);
        WebElement startDateButton = currentWeek.findElement(startDate);

        isCreatedForFirstDay = isElementsGetInByXCoordinates(appointmentWebElement, startDateButton);
        if (!isCreatedForFirstDay) {
            Reporter.log("Appointment and Day don't match ");
            return false;
        }
        if (startDateTime.getMonth().getValue() < endDateTime.getMonth().getValue()) {
            nextCalendarButton.click();
            currentWeek = calendarWeeks.get(getUSWeekOfMonth(endDateTime) - 1);
        }
        appointmentWebElement = currentWeek.findElement(appointment);
        WebElement endDateButton = currentWeek.findElement(endDate);
        isCreatedForLastDay = isElementsGetInByXCoordinates(appointmentWebElement, endDateButton);
        return isCreatedForLastDay;
    }

    private boolean checkAppointmentInMoreSection(String titleOfAppointment, LocalDateTime localDateTime) throws Exception {
        boolean isPresent;
        By moreCells = By.xpath(".//div[@class='rbc-row'][last()]//div[@class='rbc-row-segment']");
        By moreButton = By.xpath(".//span[text()='More']");
        WebElement dayOfWeekCells = getListOfElement(By
                .xpath(String.format("//div[descendant::div/button[text()='%d'] and @class='rbc-row-content']", localDateTime.getDayOfMonth()))).get(0);
        List<WebElement> moreSegments = dayOfWeekCells.findElements(moreCells);
        moreSegments.get(getUSDayOfWeek(localDateTime) - 1).findElement(moreButton).click();
        isPresent = calendarMoreEvents.stream().anyMatch(e -> e.getText().contains(titleOfAppointment));
        return isPresent;
    }

    private List<WebElement> getListOfElement(By locator) throws Exception {
        return Browser.getDriver().findElements(locator);
    }

    private boolean isElementsGetInByXCoordinates(WebElement webElement, WebElement getInWebElement) {
        boolean isGetIn = false;
        int webElementX, webElementLastX;
        int getInWebElementX, getInWebElementLastX;
        webElementX = webElement.getLocation().getX();
        webElementLastX = webElementX + webElement.getSize().getWidth();
        getInWebElementX = getInWebElement.getLocation().getX();
        getInWebElementLastX = getInWebElementX + getInWebElement.getSize().getWidth();
        if (webElementX <= getInWebElementX && webElementLastX >= getInWebElementLastX) {
            isGetIn = true;
        }
        return isGetIn;
    }


}
