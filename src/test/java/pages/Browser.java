package pages;

import io.github.bonigarcia.wdm.managers.ChromeDriverManager;
import io.github.bonigarcia.wdm.managers.FirefoxDriverManager;
import io.github.bonigarcia.wdm.managers.SafariDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.Reporter;

import static utils.Constants.browserName;
import static utils.property.Properties.getIsRemote;

public class Browser {
    private static WebDriver driver;

    private static void setDriver() throws Exception {
        switch (browserName) {
            case "Chrome":
                ChromeOptions options = new ChromeOptions();
                options.addArguments("window-size=1920x1080");
                if (getIsRemote()) {
                    options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
                    options.addArguments("disable-infobars"); // disabling info bars
                    options.addArguments("--disable-extensions"); // disabling extensions
                    options.addArguments("--no-sandbox");
                    options.addArguments("--headless");
                }
                ChromeDriverManager.getInstance().setup();
                Browser.driver = new ChromeDriver(options);
                break;
            case "Firefox":
                FirefoxDriverManager.getInstance().setup();
                Browser.driver = new FirefoxDriver();
                break;
            default:
                try {
                    throw new Exception(browserName + " is Unsupported browser");
                } catch (Exception ignored) {
                }
        }

    }

    public static WebDriver getDriver() throws Exception {
        if (Browser.driver == null) {
            setDriver();
        }
        return Browser.driver;
    }

    public static void quit() {
        Browser.driver.manage().deleteAllCookies();
        try {
            Browser.driver.quit();
        } catch (UnreachableBrowserException eQuit) {
            Thread.currentThread().interrupt();
            Browser.driver.quit();
        }
        Browser.driver = null;
    }
}
