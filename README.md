# SteerDemo

Java steerDemo is a project for UI automation.

## Tools

* Maven
* TestNG
* Selenium Webdriver

## Requirements

In order to use this project you need to have the following installed tools:

* Maven 3
* Chrome (tests use Chrome by default, can be changed in config)
* Java 11
* Git

## Usage

To start the project you need to clone it from repository:
`git clone git@gitlab.com:VikOles/SteerDemo.git`
Don't forget to add SSH public key.

To run tests from commandline navigate to root project directory and run:
`mvn compile test`
It executes tests with settings from `testng.xml`

To see test execution on browser on local machine change property `isRemote` in `config.properties` to `false`

## Reporting

Report is written into `/target/surefline-reports/` directories after a successful run.
To look on it open `index.html` or `emailable-report.html`.